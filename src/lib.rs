// Copyright 2018 Ciro DE CARO
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate serde_derive;

mod models;
pub use self::models::mutables::var;
pub use self::models::uuid;

#[cfg(test)]
mod var_tests {
    extern crate regex;
    use self::regex::RegexBuilder;

    #[test]
    fn create_var_has_id() {
        let regex = match RegexBuilder::new(
            r"^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$",
        )
        .case_insensitive(true)
        .build()
        {
            Ok(r) => r,
            Err(e) => {
                println!("Could not compile regex: {}", e);
                return;
            }
        };
        let value = String::from("test");
        let var = super::var::new(value.clone());
        let id_string = &var.id.id.to_hyphenated().to_string();
        assert!(regex.is_match(&id_string));
    }
    #[test]
    fn create_var_string() {
        let value = String::from("test");
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }

    // SIGNED INTEGERS
    #[test]
    fn create_var_i8() {
        let value: i8 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_i16() {
        let value: i16 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_i32() {
        let value: i32 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_i64() {
        let value: i64 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_i128() {
        let value: i128 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_isize() {
        let value: isize = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }

    // UNSIGNED INTEGERS
    #[test]
    fn create_var_u8() {
        let value: u8 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_u16() {
        let value: u16 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_u32() {
        let value: u32 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_u64() {
        let value: u64 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_u128() {
        let value: u128 = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_usize() {
        let value: usize = 10;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_decimal() {
        let value: i32 = 98_222;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_hex() {
        let value: i32 = 0xff;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_octal() {
        let value: i32 = 0o77;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_binary() {
        let value: i32 = 0b1111_0000;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    #[test]
    fn create_var_byte() {
        let value: u8 = b'A';
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }

    // FLOAT TYPE TEST
    #[test]
    fn create_var_float() {
        let value = 10.42;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    // BOOL TYPE TEST
    #[test]
    fn create_var_bool() {
        let value = true;
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }
    // CHAR TYPE TEST
    #[test]
    fn create_var_char() {
        let value: char = '😻';
        let var = super::var::new(value.clone());
        assert_eq!(&var.value, &value);
    }

    // JSON tests
    #[test]
    fn serialize_json() {
        let expected_var = "{\"id\":{\"id\":\"0f19b5c2-c411-40c2-9f5c-9cb811acad13\"},\"value\":\"test\",\"mutable\":true}";
        let var = super::var::Var::new("0f19b5c2-c411-40c2-9f5c-9cb811acad13", "test");
        let json = super::serde_json::to_string(&var).unwrap();

        println!("{}", json);
        assert_eq!(json, expected_var);
    }
}
