// Copyright 2018 Ciro DE CARO
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
extern crate uuid;

use self::uuid::Uuid;
use serde::de::{self, Deserialize, Deserializer, MapAccess, SeqAccess, Visitor};
use serde::ser::{Serialize, SerializeStruct, Serializer};
use std::fmt;

#[derive(PartialEq, Debug)]
pub struct UUID {
    pub id: Uuid,
}

impl UUID {
    fn new(id: &str) -> UUID {
        let id = Uuid::parse_str(id).unwrap();
        UUID { id: id }
    }
    pub fn parse_str(value: &str) -> UUID {
        let id = Uuid::parse_str(value).unwrap();
        let id = id.to_hyphenated().to_string();
        UUID::new(&id)
    }
}

pub fn new() -> UUID {
    let id = Uuid::new_v4();
    UUID { id: id }
}

impl Serialize for UUID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        // must return the number of fields in the struct
        let mut state = serializer.serialize_struct("UUID", 1)?;
        state.serialize_field("id", &self.id.to_hyphenated().to_string())?;
        state.end()
    }
}

impl<'de> Deserialize<'de> for UUID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "lowercase")]
        enum Field {
            Id,
        };

        // implementing the Visitor for UUID
        struct UUIDVisitor;

        impl<'de> Visitor<'de> for UUIDVisitor {
            type Value = UUID;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct UUID")
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<UUID, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let id = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                Ok(UUID::new(id))
            }

            fn visit_map<V>(self, mut map: V) -> Result<UUID, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut id = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Id => {
                            if id.is_some() {
                                return Err(de::Error::duplicate_field("id"));
                            }
                            id = Some(map.next_value()?);
                        }
                    }
                }
                let id = id.ok_or_else(|| de::Error::missing_field("id"))?;
                Ok(UUID::new(id))
            }
        }

        const FIELDS: &'static [&'static str] = &["Id"];
        deserializer.deserialize_struct("UUID", FIELDS, UUIDVisitor)
    }
}

#[cfg(test)]
mod uuid_test {
    use models::uuid;
    use serde_json;
    // JSON tests
    #[test]
    fn serialize_json() {
        let expected_json = "{\"id\":\"0f19b5c2-c411-40c2-9f5c-9cb811acad13\"}";
        let base_id = "0f19b5c2-c411-40c2-9f5c-9cb811acad13";
        let id = uuid::UUID::new(base_id);
        let json = serde_json::to_string(&id).unwrap();

        assert_eq!(json, expected_json);
    }
    #[test]
    fn deserialize_json() {
        let id = uuid::new();
        let json = serde_json::to_string(&id).unwrap();

        // Convert the JSON string back to a UUID.
        let deserialized: uuid::UUID = serde_json::from_str(&json).unwrap();

        assert_eq!(deserialized, id);
    }
}
