use models::operator::*;

pub type Add = OperatorSymbol;

impl<'a> Operator<&'a str> for Add {
    type Item = String;

    fn apply(left: &str, right: &str) -> String {
        let mut res = left.to_string();
        res.push_str(right);
        res
    }
}
impl Operator<f64> for Add {
    type Item = f64;

    fn apply(left: f64, right: f64) -> f64 {
        left + right
    }
}

#[cfg(test)]
mod add_tests {
    use super::*;

    #[test]
    fn test_apply_str() {
        assert_eq!(
            format!("{}", Add::apply("Hello,", " World!"),),
            String::from("Hello, World!")
        )
    }
    #[test]
    fn test_apply_f64() {
        assert_eq!(Add::apply(15.00, 2.0), 17.00)
    }
}
