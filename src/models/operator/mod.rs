use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result;

pub mod add;
pub mod divide;
pub mod multiply;
pub mod substract;

#[derive(Debug)]
pub enum OperatorSymbol {
    Add,
    Substract,
    Divide,
    Multiply,
}

impl Display for OperatorSymbol {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{:?}", self)
    }
}

pub trait Operator<T> {
    type Item;

    fn apply(left: T, right: T) -> Self::Item;
}
